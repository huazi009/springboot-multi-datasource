package com.xh.controller;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xh.entity.SysUser;
import com.xh.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统用户 前端控制器
 * </p>
 *
 * @author xiaohe
 * @since 2019-06-04
 */
@RestController
@RequestMapping("/sys-user")
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;

    @GetMapping("test1")
    public SysUser test1(){
        return sysUserService.findUserByFirstDb(1);
    }


    @GetMapping("test2")
    public SysUser test2(){
        return sysUserService.findUserBySecondDb(1);
    }


}
